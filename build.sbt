
name := "FunctionalProgramming"

version := "0.1"

scalaVersion := "2.12.4"

libraryDependencies += "org.scala-lang.modules" %% "scala-xml" % "1.0.6"
// https://mvnrepository.com/artifact/log4j/log4j
libraryDependencies += "log4j" % "log4j" % "1.2.17"
libraryDependencies += "com.typesafe.play" %% "play-json" % "2.6.7"
