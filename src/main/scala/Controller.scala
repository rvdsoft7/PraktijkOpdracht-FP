import java.io.{BufferedWriter, File, FileWriter}
import java.util
import java.util.concurrent.{Callable, Executors}

import alphabet.{Alphabet, AlphabetMap}
import ngram.{NGramCacheItem, NGrammingGenerator}
import org.apache.log4j.{Logger, PropertyConfigurator}
import statistics.{StatisticsImpl, StatisticsResult}

import scala.collection.JavaConversions.iterableAsScalaIterable
import scala.collection.JavaConverters.{asJavaIteratorConverter, seqAsJavaListConverter}
import scala.collection.mutable
import scala.io.Source
import scala.util.Random
import play.api.libs.json.Json

class Controller {
  val LOGPATH : String = new File("src/main/resources/log4j.properties").getAbsolutePath
  val CORPUS_PATH = "Corpus"
  val logger = Logger.getLogger("Controller")
  val alphabetMap = new AlphabetMap
  val generated_data_file = "gen_dat.json"
  private[this] val ALPHABET_XML = "alphabets.xml"

  def initialize: Unit = {
    PropertyConfigurator.configure(LOGPATH)
    alphabetMap.init(ALPHABET_XML)
  }


  def readFile(f: File): String ={
   Source.fromFile(f).getLines.mkString
  }

  def loadStats(forceGen: Boolean, testMode: Boolean) : Map[String,  Map[String, List[(String, Int)]]] = {
    logger.info(s"trying to loadStats( forceGen: $forceGen)")
    val f = new File(generated_data_file)
    if(!forceGen && f.exists()) {
      val json = readFile(f)
     val result = Json.fromJson[Map[String, Map[String, List[(String, Int)]]]](Json.parse(json)).getOrElse(null)
      if(result!=null) {
        logger.info(s"successfully loaded stats from local file: ${f.getName}")
        return result
      } else {
        throw new RuntimeException(s"Failed to loadStats from local file: ${f.getName}")
      }
    }
    logger.info(s"starting stats generations... testMode:$testMode")
    val taalMap = new mutable.HashMap[String,  Map[String, List[(String, Int)]]]()
    getCorpusLangs().foreach(l=> {

      val cache = new NGramCacheItem(new NGrammingGenerator(alphabetMap.getAlphabet(l)))
      val data = Source.fromFile(getFileName(l)).getLines.toList.filter(x=> !x.isEmpty)
      //cut data in parts and generate concurrently to speed up the process
      //afterwards we merge the data, cache it and return it
      val threadPoolSize = 20
      val takeLimitTestMode = 1000
      val executorService = Executors.newFixedThreadPool(threadPoolSize)
      val cutSize = threadPoolSize
      val dataToBeUsed = if(testMode) Random.shuffle(data) take(takeLimitTestMode) else data
      val cutData = dataToBeUsed.grouped(dataToBeUsed.size/cutSize).toList
      val mapList = new util.ArrayList[mutable.HashMap[String, List[(String, Int)]]]
      if(!testMode) {
        if(cutData.map(x=>x.length).sum != data.length) {
          throw new RuntimeException("cutData.map(x=>x.length).sum != data.length")
        }
      }
      logger.info(s"Starting stats generations for $l. threadPoolSize: $threadPoolSize, sizeTrigrams:${cache.trigrams.length}")
      executorService invokeAll (cutData map(c => new StatisticsResult(new StatisticsImpl(cache, c)))).toList.asJava forEach(x=>mapList.add(x.get))
      executorService.shutdown()
      logger.info(s"Successfully generated stats for $l")
      val totalMap = mapList .flatten
        .groupBy { case (k, v) => k }
        .mapValues { _.map { case (k, v) => v } } mapValues(list=>list.reduce(_++_) groupBy(x=>x._1) map( kv => (kv._1, kv._2.map( _._2).sum))) mapValues (x=>x.toList)
   val finalMap = totalMap.map{case (k, v) => k -> (
        if (List("wordsStartingWithLetterCount", "wordsEndingWithLetterCount", "wordsLetterFrequency").contains(k)) v.sortWith(_._1<_._1)
        else if (List("wordsStartingWithBigramCount", "wordsEndingWithBigramCount").contains(k)) v.sortWith(_._2>_._2) take 50
        else if (List("wordsTrigramsFrequency", "wordsSkipgramsFrequency", "wordsBigramsFrequency").contains(k)) v.sortWith(_._2>_._2) take 25
        else v)}
      finalMap foreach {case (key, value) => logger.info (key + "-->" + value)}
     taalMap.put(l, finalMap)
    })
    writeFile(f, Json.toJson(taalMap).toString())
    logger.info(s"wrote all stats to ${f.getName}")
   return loadStats(false, testMode)
  }

  def writeFile(f: File, text: String): Unit = {
    val bw = new BufferedWriter(new FileWriter(f))
    bw.write(text)
    bw.close()
  }

  def getFileName(l :String): String = {
    CORPUS_PATH+"/"+l+".txt"
  }

  def getCorpusLangs(): util.List[String] = {
    val list = new util.ArrayList[String]
    for (lang <- new File(CORPUS_PATH).listFiles()) {
      val fileName : String = lang.getName
      list.add(fileName.substring(0, fileName.indexOf('.')))
    }
    list
  }
}
