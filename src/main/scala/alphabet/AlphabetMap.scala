package alphabet

import java.util
import java.util.Locale

import org.apache.log4j.Logger

import scala.collection.convert.ImplicitConversionsToScala.`list asScalaBuffer`
import scala.collection.mutable
import scala.xml.{Node, XML}

class AlphabetMap {

  private[this] val map = new mutable.HashMap[String, Alphabet]()
  private[this] val logger = Logger.getLogger("alphabet.AlphabetMap")

  private[this] def getXmlValue(xml: Node, key : String): String = {
    (xml \ key).text.trim
  }

 def init(input : String): Unit = {
    val xml = XML.load(input)
    (xml \ "alphabet").toList foreach addLanguage
  }

  def getAlphabet(language: String): Alphabet = {
    map.get(language).get
  }

  private[this] def addLanguage(alphabetXml: Node): Unit = {
    val language = getXmlValue(alphabetXml, "language")
    logger.info(s"addLanguage: $language")
    val complete = getXmlValue(alphabetXml, "complete")
    val vowels = getXmlValue(alphabetXml, "vowels")
    val consonants = getXmlValue(alphabetXml, "consonants")
    map.put(language, new Alphabet(language, complete,
      vowels, consonants))
  }
}
