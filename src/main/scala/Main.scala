import java.io.File
import java.util
import java.util.Locale
import java.util.logging.Logger
import javafx.application.Application
import javafx.scene.control.{Tab, TabPane}
import javafx.scene.paint.Color
import javafx.scene.shape.Rectangle
import javafx.stage.Stage


object Main {

  def main(args: Array[String]) {
    Application.launch(classOf[UIApplication], args: _*)
  }

}
