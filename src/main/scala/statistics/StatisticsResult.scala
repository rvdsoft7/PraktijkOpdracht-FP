package statistics

import java.util.concurrent.Callable

import org.apache.log4j.Logger

import scala.collection.mutable

class StatisticsResult(val stats : IStatistics) extends Callable[mutable.HashMap[String, List[(String, Int)]]]{


  val logger = Logger.getLogger("StatisticsResult")

  logger.info("created StatisticsResult")

  override def call() = {
    logger.info("call")
    val map = new mutable.HashMap[String, List[(String, Int)]]()
    val statisticsClass = stats.getClass
    val statisticsInterfaceClass = statisticsClass.getSuperclass
    statisticsInterfaceClass.getDeclaredMethods.map(m => m.getName).foreach(methodName => {
      val method = statisticsClass.getMethod(methodName)
      method.setAccessible(true)
      val value = method.invoke(stats)
      map.put(methodName, value match { case v: List[(String, Int)] => v })
    })
    map
  }
}
