package statistics

import ngram.NGramCacheItem

import scala.util.matching.Regex

class StatisticsImpl(cache: NGramCacheItem, data: List[String]) extends IStatistics {

  val alphabet = cache.alphabet
  val dataToString = data.mkString(",")

 override def wordsStartingWithBigramCount:  List[(String, Int)]= {
   cache.bigrams map (x=> (x, data.count(y=>y.startsWith(x))))
  }

  override def wordsEndingWithBigramCount:  List[(String, Int)] = {
    cache.bigrams map (x=> (x, data.count(y=>y.endsWith(x))))
  }

  override  def wordsLetterFrequency:  List[(String, Int)] = {
    cache.unigrams map (x=> (x, dataToString count(y=>x.charAt(0)==y.charValue())))
  }

  override def wordsVowelsConsonantsFrequency: List[(String, Int)] = {
    wordsLetterFrequency.map(tupple=> (tupple, alphabet.vowels.contains(tupple._1))).groupBy(x=>x._2)
      .map{ case (key, value) => (if (key) "vowels" else "consonants") -> value.map(v => v._1._2).sum }
      .toList
  }

  override  def wordsBigramsFrequency: List[(String, Int)] = {
    wordsStringFrequency(cache.bigrams)
  }

  override def wordsTrigramsFrequency: List[(String, Int)] = {
    wordsStringFrequency(cache.trigrams)
  }

  override  def wordsSkipgramsFrequency: List[(String, Int)] = {
    wordsStringFrequency(cache.skipTrigrams)
  }

  override  def wordsStartingWithLetterCount :  List[(String, Int)] = {
    cache.unigrams map (x=> (x, data.count(y=>y.startsWith(x))))
  }

  override  def wordsEndingWithLetterCount :  List[(String, Int)] = {
    cache.unigrams map (x=> (x,  data.count(y=>y.endsWith(x))))
  }

  private [this] def wordsStringFrequency(list: List[String]): List[(String, Int)] = {
    list map (x=> (x, regexCheck(x, dataToString)))
  }

  private [this] def regexCheck(toFind: String, data: String): Int = {
    new Regex(toFind).findAllMatchIn(data).length
  }
}
