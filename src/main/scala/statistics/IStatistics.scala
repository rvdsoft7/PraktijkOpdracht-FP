package statistics

abstract class IStatistics {
  /**
    * vraag 5
    * @return
    */
  def wordsStartingWithBigramCount:  List[(String, Int)]

  /**
    * vraag 6
    * @return
    */
  def wordsEndingWithBigramCount:  List[(String, Int)]

  /**
    * vraag 3
    * @return
    */
  def wordsLetterFrequency:  List[(String, Int)]
  /**
    * vraag 4
    * @return
    */
  def wordsVowelsConsonantsFrequency: List[(String,Int)]

  /**
    * vraag 7
    * @return
    */
  def wordsBigramsFrequency: List[(String, Int)]

  /**
    * vraag 7
    * @return
    */
  def wordsTrigramsFrequency: List[(String, Int)]

  /**
    * vraag 8
    * @return
    */
  def wordsSkipgramsFrequency: List[(String, Int)]

  /**
    * vraag 2
    * @return
    */
  def wordsStartingWithLetterCount :  List[(String,Int)]

  /**
    * vraag 1
    * @return
    */
  def wordsEndingWithLetterCount :  List[(String, Int)]
}
