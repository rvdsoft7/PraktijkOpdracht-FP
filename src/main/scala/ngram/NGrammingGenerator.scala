package ngram

import alphabet.Alphabet


class NGrammingGenerator(val alphabet: Alphabet) {

  def unigrams(): List[String]  = {
    ngrams(1)
  }

  def bigrams(): List[String]  = {
    ngrams(2)
  }

  def trigrams(): List[String]  = {
    ngrams(3)
  }

  def skipTrigrams(): List[String] = {
    skipGrams(3, 1)
  }


  /**
    *
    * @param n size of grams
    * @param skip index of char to skip
    * @return
    */
  private [this]  def skipGrams(n: Int, skip:Int): List[String] = {
    combinations(n, alphabet).map(x=> {
       x.updated(skip, '.').mkString
    }).distinct
  }



  private [this] def ngrams(n: Int): List[String] = {
    combinations(n, alphabet).map(x=>x.mkString)
  }

 private[this] def combinations(size: Int, alphabet: Alphabet) : List[List[Char]] = {
    if (size == 0)
      List(List())
    else {
      for {
        x  <- alphabet.complete.toList
        xs <- combinations(size-1, alphabet)
      } yield x :: xs
    }
  }
}
