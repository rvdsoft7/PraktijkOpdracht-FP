package ngram

class NGramCacheItem(gen : NGrammingGenerator) {
  val alphabet = gen.alphabet
  val unigrams = gen.unigrams()
  val bigrams = gen.bigrams()
  val trigrams = gen.trigrams()
  val skipTrigrams = gen.skipTrigrams()
}
