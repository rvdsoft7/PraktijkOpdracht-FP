import javafx.beans.value.{ChangeListener, ObservableValue}
import javafx.collections.{FXCollections, ObservableList}
import javafx.event.ActionEvent
import javafx.geometry.Insets
import javafx.scene.{Node, Parent}
import javafx.scene.chart._
import javafx.scene.control._
import javafx.scene.layout.VBox
import javafx.scene.text.Font

import org.apache.log4j.Logger

class UIHelper(stats: Map[String, Map[String, List[(String, Int)]]]) {


  val tabPane: TabPane = new TabPane
  val logger = Logger.getLogger("UIHelper")
  val vraag1 = "Per letter het aantal woorden dat er mee begint"
  val vraag2 = "Per letter het aantal woorden dat er mee eindigt"
  val vraag3 = "Per letter de frequentie over de hele lijst"
  val vraag4 = "Frequentie van de vowels en de consonants als groep over de hele lijst"
  val vraag5 = "Per bigram het aantal woorden dat er mee begint"
  val vraag6 = "Per bigram het aantal woorden dat er mee eindigt"
  val vraag7_bigram = "Frequentie van de bigrams"
  val vraag7_trigram = "Frequentie van de trigrams"
  val vraag8 = "Frequentie van de skipgrams"
  val labelMap = Map("wordsStartingWithBigramCount" -> vraag5,
    "wordsEndingWithBigramCount" -> vraag6,
    "wordsLetterFrequency" -> vraag3,
    "wordsVowelsConsonantsFrequency" -> vraag4,
    "wordsBigramsFrequency" -> vraag7_bigram,
    "wordsTrigramsFrequency" -> vraag7_trigram,
    "wordsSkipgramsFrequency" -> vraag8,
    "wordsStartingWithLetterCount" -> vraag2,
    "wordsEndingWithLetterCount" -> vraag1)

  def getSortingValue(vraag: String): Integer = {
    vraag match {
      case `vraag1` => 1
      case `vraag2` => 2
      case `vraag3` => 3
      case `vraag5` => 5
      case `vraag6` => 6
      case `vraag4` => 4
      case `vraag7_bigram` => 7
      case `vraag7_trigram` => 8
      case `vraag8` => 9
    }
  }


  private def initTabPane(): Unit = {
    tabPane.getSelectionModel.selectedItemProperty().addListener(new ChangeListener[Tab] {
      override def changed(observable: ObservableValue[_ <: Tab], oldValue: Tab, newValue: Tab): Unit = {
        logger.info(s"Showing: ${newValue.getText}")
        if (oldValue!=null && oldValue.getContent != null) {
          val parent = oldValue.getContent.asInstanceOf[Parent]
          if (parent.lookup("#mainnav") != null) {
            setMainNavListToSelectedTab
          } else if (parent.lookup("#analyse") != null) {
            val label = parent.lookup("#analyse").asInstanceOf[Label].getText
            createDetailViewAndBind(label)
          }
        }
      }
    })

    for (lang <- stats.keySet) {
      logger.info(s"Building tab for $lang")
      val tab: Tab = new Tab
      tab.setText(lang)
      tab.setClosable(false)
      tabPane.getTabs.add(tab)
    }

    setMainNavListToSelectedTab()
  }

  initTabPane()

  def setMainNavListToSelectedTab(): Unit = {
    tabPane.getSelectionModel.getSelectedItem.setContent(createMainNavList())
  }

  def createMainNavList(): Node = {
    val listview = getListView(labelMap.values.toList map (x => (x, getSortingValue(x))) sortBy (y => y._2) map (z => z._1))
    listview.setId("mainnav")
    listview.setCellFactory((cell) => {
      new ListCell[String]() {
        override protected def updateItem(item: String, empty: Boolean): Unit = {
          super.updateItem(item, empty)
          if (item != null) {
            setText(item)
            setFont(Font.font(16))
          }
        }
      }
    })
    listview.setOnMouseClicked((event) => {
      val selectedItem = listview.getSelectionModel.getSelectedItem
      if (selectedItem != null) {
        createDetailViewAndBind(selectedItem)
      }
    })
    listview
  }


  def createDetailViewAndBind(selectedItem: String) {
    val tab = tabPane.getSelectionModel.getSelectedItem
    val lang = tab.getText
    val entry = stats.get(lang).get
    logger.info(s"Showing: ${selectedItem} for ${tab.getText}")
    val key = labelMap.find(_._2 == selectedItem).get._1
    val statValues = convDataToPerc(entry.get(key).get)

    def bindToTab(layout: Node*) = tab.setContent(createLayout(selectedItem, layout: _*))

    // Your action here
    selectedItem match {
      case `vraag1` => bindToTab(getListView1(statValues))
      case `vraag2` => bindToTab(getListView1(statValues))
      case `vraag3` => bindToTab(getListView1(statValues))
      case `vraag5` => bindToTab(getListView1(statValues))
      case `vraag6` => bindToTab(getListView1(statValues))
      case `vraag4` => bindToTab(getPieChart(statValues))
      case `vraag7_bigram` => bindToTab(getHistogram(statValues))
      case `vraag7_trigram` => bindToTab(getHistogram(statValues))
      case `vraag8` => bindToTab(getHistogram(statValues))
    }
  }

  def getPieChart(data: List[(String, Float)]): Node = {
    val pieChartData: ObservableList[PieChart.Data] = FXCollections.observableArrayList()
    data map (x => new PieChart.Data(x._1, x._2)) foreach pieChartData.add
    val chart = new PieChart(pieChartData)
    chart
  }

  def getHistogram(data: List[(String, Float)]): Node = {
    val xAxis = new CategoryAxis()
    val yAxis = new NumberAxis()
    val barChart = new BarChart(xAxis, yAxis)
    val dataSeries1: XYChart.Series[String, Number] = new XYChart.Series[String, Number]
    data foreach (x => dataSeries1.getData.add(new XYChart.Data[String, Number](x._1, x._2)))
    barChart.getData.add(dataSeries1)
    barChart
  }

  def createLayout(label: String, layout: Node*): Node = {
    val root = new VBox(3)
    root.setPadding(new Insets(10))
    val textView = new Label(label)
    textView.setId("analyse")
    textView.setFont(new Font(17))
    val backButton = new Button("Terug")
    backButton.setOnAction((e: ActionEvent) => {
      setMainNavListToSelectedTab
    })
    root.getChildren.addAll(backButton, textView)
    root.getChildren.addAll(layout: _*)
    root
  }

  def convDataToPerc(data: List[(String, Int)]): List[(String, Float)] = {
    val total = data.map(x => x._2).sum
    data map (x => (x._1, x._2.toFloat / total.toFloat* 100.0f))
  }

  def getListView1(data: List[(String, Float)]): ListView[String] = {
    getListView(data map (x => f"${x._1}    ${x._2}%.2f%%"))
  }

  def getListView(data: List[(String)]): ListView[String] = {
    val list = new ListView[String]
    val items: ObservableList[String] = FXCollections.observableArrayList()
    data foreach items.add
    list.setItems(items)
    list.setPrefWidth(300)
    list.setPrefHeight(500)
    list
  }
}
