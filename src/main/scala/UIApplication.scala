
import java.lang
import javafx.application.Application
import javafx.beans.value.{ChangeListener, ObservableValue}
import javafx.collections.{FXCollections, ObservableList}
import javafx.event.{ActionEvent, Event, EventHandler}
import javafx.geometry.Insets
import javafx.scene.chart._
import javafx.scene.{Node, Scene}
import javafx.scene.control._
import javafx.scene.input.MouseEvent
import javafx.scene.layout.{StackPane, VBox}
import javafx.scene.paint.Color
import javafx.scene.shape.Rectangle
import javafx.scene.text.Font
import javafx.stage.Stage

import org.apache.log4j.Logger

import scala.collection.JavaConverters.asJavaIterableConverter
import scala.collection.mutable

class UIApplication extends Application {
  val forceGenDat = false
  val testMode = false
  val logger = Logger.getLogger("UIApplication")

  override def start(primaryStage: Stage): Unit = {
    val controller = new Controller
    controller.initialize
    val stats = controller.loadStats(forceGenDat, testMode)
    primaryStage.setTitle("PraktijkOpdrachtFP")
    logger.info("starting to build UI...")
    val root = new StackPane
    root.getChildren.add(new UIHelper(stats).tabPane)
    primaryStage.setScene(new Scene(root, 1000, 600))
    primaryStage.show

  }


}
